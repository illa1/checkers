from PyQt5 import QtWidgets, QtCore, QtGui

DEAFULT_SIZE = 70

class Board(QtWidgets.QWidget):

    def __init__(self, game):
        super(Board, self).__init__()
        self.game = game
        self.size = DEAFULT_SIZE
        self.setFixedSize(self.size*8, self.size*8)

    def paintEvent(self, e):
        self.board = QtGui.QPainter()
        self.board.begin(self)
        self.printBoard()
        self.board.end()

    def mousePressEvent(self, e):
        if self.game.player_move:
            x, y = e.pos().x(), e.pos().y()
            self.game.check_field(x//self.size, y//self.size)

    def printBoard(self):
        board = self.board
        light = QtGui.QColor("#FFCC66")
        dark = QtGui.QColor("#B33C00")
        white = QtGui.QColor("#FFFFFF")
        black = QtGui.QColor("#000000")
        white_queen = QtGui.QColor("#969696")
        black_queen = QtGui.QColor("#646464")
        current = QtGui.QColor("#00FF00")

        grid = self.game.grid
        player_id = self.game.player_id
        current_field = self.game.current_field
        for x in range(8):
            for y in range(8):
                color_of_field = grid[y][x]
                if player_id == 1:
                    color_of_field = grid[7-y][7-x]
                color = light if (x+y) % 2 else dark
                board.setBrush(color)
                board.drawRect(x*self.size, y*self.size, self.size, self.size)
                if current_field is not None and current_field[0] == x and current_field[1] == y:
                    board.setBrush(current)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-1, self.size/2-1)
                if color_of_field == 11:
                    board.setBrush(white_queen)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-4, self.size/2-4)
                    board.setBrush(white)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-10, self.size/2-9)
                if color_of_field == 12:
                    board.setBrush(black_queen)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-4, self.size/2-4)
                    board.setBrush(black)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-10, self.size/2-9)

                if color_of_field == 1:
                    board.setBrush(white)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-4, self.size/2-4)
                if color_of_field == 2:
                    board.setBrush(black)
                    board.drawEllipse(QtCore.QPoint(x*self.size+self.size/2, y*self.size+self.size/2), self.size/2-4, self.size/2-4)
