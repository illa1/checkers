from PyQt5 import QtWidgets, QtCore, QtGui
from game import Game
from board import Board
from statistics import fromFileToJson
import sys

LEVELS_NAMES = {
    1: 'very easy',
    2: 'easy',
    3: 'normal',
    4: 'hard',
}

DEFAULT_PLAYER_ID = 1
DEFAULT_LEVEL = 3

class Window(QtWidgets.QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.settings = QtCore.QSettings("illa1", "checkers")
        player_id = DEFAULT_PLAYER_ID
        self.level = int(self.settings.value("level", DEFAULT_LEVEL))
        self.game = Game(player_id, self.level)
        self.change_color = bool(self.settings.value("change_color", False))
        self.initUI()
        self.game.start_new_game(False)

    def initUI(self):

        mainMenu = self.menuBar()
        mainMenu.setNativeMenuBar(False)
        menu = mainMenu.addMenu('Game')

        new_game_button = QtWidgets.QAction('New Game', self)
        new_game_button.triggered.connect(self.new_game_dialog)
        menu.addAction(new_game_button)

        settingsButton = QtWidgets.QAction('Settings', self)
        settingsButton.triggered.connect(self.settings_dialog)
        menu.addAction(settingsButton)

        statisticsButton = QtWidgets.QAction('Statistics', self)
        statisticsButton.triggered.connect(self.statistics_dialog)
        menu.addAction(statisticsButton)

        board = Board(self.game)
        self.game.board = board
        self.setCentralWidget(board)
        self.setWindowTitle('Checkers')
        self.setFixedSize(self.sizeHint())

        self.show()

    def new_game_dialog(self):
        dialog = QtWidgets.QMessageBox(self)
        dialog.setWindowTitle('New game')
        dialog.setText('Do you want to start a new game?')
        dialog.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        cb = QtWidgets.QCheckBox('Change color')
        dialog.setCheckBox(cb)
        if self.change_color:
            cb.toggle()
        answer = dialog.exec_()
        last_change_color = self.change_color
        if answer == QtWidgets.QMessageBox.Yes:
            if cb.isChecked():
                self.change_color = True
            else:
                self.change_color = False
            self.game.start_new_game(self.change_color)
            if self.change_color != last_change_color:
                self.settings.setValue("change_color", self.change_color)

    def settings_dialog(self):
        dialog = SettingsDialog(self, self.level)
        result = dialog.exec_()
        if result > 0:
            self.level = result
            self.game.level = self.level
            self.game.start_new_game(False)
            self.settings.setValue("level", self.level)

    def statistics_dialog(self):
        dialog = StatisticsDialog(self)
        dialog.exec_()

    def closeEvent(self, e):
        self.game.close()

class SettingsDialog(QtWidgets.QDialog):

    label_text = 'Level of AI: {}'

    def __init__(self, parent, level):
        super(SettingsDialog, self).__init__(parent)
        self.setWindowTitle('Settings')
        layout = QtWidgets.QVBoxLayout(self)
        label = QtWidgets.QLabel(SettingsDialog.label_text.format(LEVELS_NAMES[level]))
        slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        slider.setMinimum(1)
        slider.setMaximum(4)
        slider.setValue(level)
        slider.valueChanged.connect(self.update_label)
        buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Save)
        buttons.button(QtWidgets.QDialogButtonBox.Cancel).clicked.connect(self.cancel)
        buttons.button(QtWidgets.QDialogButtonBox.Save).clicked.connect(self.save)
        layout.addWidget(label)
        layout.addWidget(slider)
        layout.addWidget(buttons)
        self.setLayout(layout)
        self.label = label
        self.slider = slider
        self.setFixedSize(self.sizeHint())

    def save(self, button):
        self.done(self.slider.value())

    def cancel(self, button):
        self.done(0)

    def update_label(self):
        self.label.setText(SettingsDialog.label_text.format(LEVELS_NAMES[self.slider.value()]))

DEAFULT_CHART_WIDTH = 200
DEAFULT_CHART_HEIGHT = 80
DEAFULT_CHART_MARGIN = 5

class Chart(QtWidgets.QWidget):
    def __init__(self, record):
        super(Chart, self).__init__()
        self.record = record
        self.width = DEAFULT_CHART_WIDTH
        self.height = DEAFULT_CHART_HEIGHT
        self.setFixedSize(self.width, self.height+DEAFULT_CHART_MARGIN)

    def paintEvent(self, e):
        won_color = QtGui.QColor("#ccff99")
        lost_color = QtGui.QColor("#ff9966")
        black_color = QtGui.QColor("#000000")
        p = QtGui.QPainter()
        p.begin(self)
        won = self.record['won']
        lost = self.record['lost']
        all_games = won + lost
        if all_games > 0:
            won_percent = 1.0 * won / all_games
            lost_percent = 1.0 * lost / all_games
            p.setBrush(won_color)
            p.drawRect(DEAFULT_CHART_MARGIN, self.height*(1-won_percent), self.width/2 - 2*DEAFULT_CHART_MARGIN, self.height*won_percent)
            p.setBrush(lost_color)
            p.drawRect(self.width/2 + DEAFULT_CHART_MARGIN, self.height*(1-lost_percent), self.width/2 - 2*DEAFULT_CHART_MARGIN, self.height*lost_percent)
        p.setBrush(black_color)
        p.drawText(QtCore.QPoint(self.width/4 - 2*DEAFULT_CHART_MARGIN, self.height - 2*DEAFULT_CHART_MARGIN), '{} won'.format(won))
        p.drawText(QtCore.QPoint(3*self.width/4 - 2*DEAFULT_CHART_MARGIN, self.height - 2*DEAFULT_CHART_MARGIN), '{} lost'.format(lost))
        p.end()


class StatisticsDialog(QtWidgets.QDialog):

    def __init__(self, parent):
        super(StatisticsDialog, self).__init__(parent)
        self.setWindowTitle('Statistics')
        results = fromFileToJson('statistics.json')
        layout = QtWidgets.QVBoxLayout(self)
        for record in results:
            label = QtWidgets.QLabel('Level: {}'.format(LEVELS_NAMES[record['level']]))
            layout.addWidget(Chart(record))
            layout.addWidget(label)
        self.setLayout(layout)
        self.setFixedSize(self.sizeHint())


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
