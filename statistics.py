from PyQt5 import QtCore
import json


DEFAULT_STATISTICS = [
    {'level' : 1, 'won': 0, 'lost': 0},
    {'level' : 2, 'won': 0, 'lost': 0},
    {'level' : 3, 'won': 0, 'lost': 0},
    {'level' : 4, 'won': 0, 'lost': 0},
]

def saveJsonInFile(data, file_name):
    directory = QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.DataLocation)
    if not QtCore.QDir(directory).exists():
        QtCore.QDir().mkdir(directory)
    file = QtCore.QFile(directory + "/" + file_name)
    file.open(QtCore.QIODevice.WriteOnly)
    file.write(json.dumps(data))
    file.close()

def fromFileToJson(file_name):
    directory = QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.DataLocation)
    if not QtCore.QDir(directory).exists() or not QtCore.QFile(directory + '/' + file_name).exists():
        return DEFAULT_STATISTICS
    file = QtCore.QFile(directory + "/" + file_name)
    file.open(QtCore.QIODevice.ReadOnly)
    return json.loads(bytes(file.readAll()).decode("utf-8"))
