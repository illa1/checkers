#ifndef __MINMAX__HPP__
#define __MINMAX__HPP__

template<typename Game, typename Move>
std::pair<std::experimental::optional<int>, Move> 
    minMax(Game& game, int depth, std::experimental::optional<int> maxFromUp,
            std::experimental::optional<int> minFromUp, int playerId) {

    if (depth == 0) {
        return std::make_pair(game.evaluate(playerId), Move());
    }
    Move best;
    std::vector<Move> moves = game.getMoves();
    std::experimental::optional<int> max;
    std::experimental::optional<int> min;
    bool player = game.isPlayerMove(playerId);
    if (! game.isNotEnd()) {
        return std::make_pair(game.evaluate(playerId), Move());
    }
    for (auto& move : moves) {
        game.makeMove(move);
        std::experimental::optional<int> value = minMax<Game, Move>(game, depth-1, max, min, playerId).first;
        game.reverseMove(move);
        if (!value) {
            continue;
        }
        if (player && (!max || max < value)) {
            max = value;
            best = move;
        }
        if (!player && (!min || min > value)) {
            min = value;
            best = move;
        }
        if (player && minFromUp && max > minFromUp) {
            break;
        }
        if (!player && maxFromUp && min < maxFromUp) {
            break;
        }
    }
    if (player) {
        return std::make_pair(max, best);
    } else {
        return std::make_pair(min, best);
    }
}

#endif
