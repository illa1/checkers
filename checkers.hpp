#ifndef __CHECKERS__HPP__
#define __CHECKERS__HPP__

#include <experimental/optional>
#include <vector>
#include <array>
#include <algorithm>


typedef std::array<int, 64> CheckersBoard;


class GameMove {

public:
    int x1;
    int x2;
    int y1;
    int y2;
    int xBeaten;
    int yBeaten;
    bool changedPlayer;
    int color;
    int colorBeaten;

    GameMove(){}
    GameMove(int x1, int y1, int x2, int y2, int xBeaten, int yBeaten)
        : x1(x1), y1(y1), x2(x2), y2(y2), xBeaten(xBeaten), yBeaten(yBeaten) {
    }

    bool operator ==(const GameMove &m) const {
        if (m.x1 != x1) return false; 
        if (m.x2 != x2) return false; 
        if (m.y1 != y1) return false; 
        if (m.y2 != y2) return false; 
        if (m.xBeaten != xBeaten) return false;
        if (m.yBeaten != yBeaten) return false;
        return true;
    }
};

class GameState {
    int turn;
    int playerColor;
    std::vector<GameMove> lastMoves;
    std::vector<std::vector<int>> board;
    
    static const int firstPlayer = 1;
    static const CheckersBoard oneDimensialInitBoard;

public:

    void init(const int whichTurn = firstPlayer, const CheckersBoard & fields = oneDimensialInitBoard);
    CheckersBoard getBoard() const;

    //for minmax algorithm:
    bool isNotEnd() const;
    bool isPlayerMove(int player) const;
    std::vector<GameMove> getMoves() const;
    std::experimental::optional<int> evaluate(int playerId) const;
    void reverseMove(const GameMove& move);
    void makeMove(GameMove& move);
    
private:

    bool isColorOfPlayer(const int color, const int playerId) const;
    bool isPlayerColor(const int color) const;
    bool isOpponentColor(const int color) const;
    int check(const int x, const int y) const;    
    void queenCheck(int x, int y, int dx, int dy, std::vector<GameMove>& moves) const;
    std::vector<GameMove> getMovesFromField(int x, int y) const;
};


#endif
