#include <gtest/gtest.h>
#include "checkers.hpp"


bool sameArrays(const CheckersBoard & a, const CheckersBoard & b) {
    if (a.size() != b.size()) {
        return false;
    }
    for (size_t i = 0; i < a.size(); i++) {
        if (a[i] != b[i]) {
            return false;
        }
    }
    return true;
}

TEST(GameStateTest, SimpleMovesTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    GameMove m = GameMove(2, 0, 1, 1, -1, -1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    m = GameMove(1, 7, 2, 6, -1, -1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, BeatTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    GameMove m = GameMove(2, 0, 4, 2, 3, 1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, DoubleBeatTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    GameMove m = GameMove(2, 0, 4, 2, 3, 1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    m = GameMove(4, 2, 2, 4, 3, 3);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, GetMovesPlayer1Test) {
    GameState game;
    CheckersBoard board = {
        0, 1, 0, 1, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 4);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(1, 0, 2, 1, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(3, 0, 2, 1, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(3, 0, 4, 1, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(0, 1, 1, 2, -1, -1)) != std::end(moves));
}

TEST(GameStateTest, GetMovesPlayer2Test) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0,
        2, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0
    };
    game.init(2, board);
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 2);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(3, 7, 2, 6, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(3, 7, 4, 6, -1, -1)) != std::end(moves));
}

TEST(GameStateTest, GetMovesPlayer1OnlyBeatsTest) {
    GameState game;
    CheckersBoard board = {
        0, 1, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0,
        2, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 2, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 2);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(3, 0, 5, 2, 4, 1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(6, 3, 4, 5, 5, 4)) != std::end(moves));
}

TEST(GameStateTest, GetMovesPlayer1QueenTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    std::vector<GameMove> moves = game.getMoves();
    GameMove m = GameMove(4, 6, 5, 7, -1, -1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0,11, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, GetMovesPlayer2QueenTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(2, board);
    std::vector<GameMove> moves = game.getMoves();
    GameMove m = GameMove(2, 1, 1, 0, -1, -1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    CheckersBoard boardAfterMoves = {
        0,12, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, GetMovesPlayer1QueenAfterBeatTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 2, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    std::vector<GameMove> moves = game.getMoves();
    GameMove m = GameMove(7, 5, 5, 7, 6, 6);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0,11, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}


TEST(GameStateTest, GetMovesPlayer1NotQueenInMiddleOfBeatTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 2, 0, 2, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    std::vector<GameMove> moves = game.getMoves();
    GameMove m = GameMove(7, 5, 5, 7, 6, 6);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    m = GameMove(5, 7, 3, 5, 4, 6);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}


TEST(GameStateTest, DoubleBeatAvailableMovesTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 2, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 2, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    GameMove m = GameMove(2, 0, 4, 2, 3, 1);
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 1);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(4, 2, 2, 4, 3, 3)) != std::end(moves));
}

TEST(GameStateTest, QueenAvailableMovesTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,11, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0,
        2, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 9);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 4, 3, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 3, 4, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 2, 5, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 6, 1, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 7, 0, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 4, 1, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 3, 0, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(5, 2, 6, 3, -1, -1)) != std::end(moves));
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), GameMove(7, 4, 6, 5, -1, -1)) != std::end(moves));
}

TEST(GameStateTest, QueenAvailableMovesHasToBeatTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,11, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    std::vector<GameMove> moves = game.getMoves();
    EXPECT_EQ(moves.size(), 1);
    GameMove m = GameMove(5, 2, 1, 6, 2, 5);
    EXPECT_TRUE(std::find(std::begin(moves), std::end(moves), m) != std::end(moves));
    game.makeMove(m);
    EXPECT_TRUE(game.isPlayerMove(2));
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 0,
        0,11, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
}

TEST(GameStateTest, ReverseSimpleMoveTest) {
    GameState game;
    CheckersBoard board = {
        0, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    GameMove m = GameMove(1, 0, 2, 1, -1, -1);
    game.makeMove(m);
    game.reverseMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

TEST(GameStateTest, ReverseBeatMoveTest) {
    GameState game;
    CheckersBoard board = {
        0, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    GameMove m = GameMove(1, 0, 3, 2, 2, 1);
    game.makeMove(m);
    game.reverseMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

TEST(GameStateTest, ReverseDoubleBeatMoveTest) {
    GameState game;
    CheckersBoard board = {
        0, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    GameMove m1 = GameMove(1, 0, 3, 2, 2, 1);
    game.makeMove(m1);
    GameMove m2 = GameMove(3, 2, 5, 4, 4, 3);
    game.makeMove(m2);
    game.reverseMove(m2);
    EXPECT_TRUE(game.isPlayerMove(1));
    CheckersBoard boardAfterMoves = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    EXPECT_TRUE(sameArrays(game.getBoard(), boardAfterMoves));
    game.reverseMove(m1);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

TEST(GameStateTest, ReverseQueenMoveTest) {
    GameState game;
    CheckersBoard board = {
        0,11, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    GameMove m = GameMove(1, 0, 5, 4, -1, -1);
    game.makeMove(m);
    game.reverseMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}


TEST(GameStateTest, ReverseBecomeQueenTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    GameMove m = GameMove(5, 6, 6, 7, -1, -1);
    game.makeMove(m);
    game.reverseMove(m);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

TEST(GameStateTest, StartGameTest) {
    GameState game;
    game.init();
    CheckersBoard board = {
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 2, 0, 2, 0, 2,
        2, 0, 2, 0, 2, 0, 2, 0,
        0, 2, 0, 2, 0, 2, 0, 2
    };
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

TEST(GameStateTest, IsEndCouseNoPiecesTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 2, 0, 0,
        0, 0, 0, 0, 2, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_FALSE(game.isNotEnd());
}

TEST(GameStateTest, IsEndCouseNoMovesTest) {
    GameState game;
    CheckersBoard board = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 2, 0, 2,
        0, 0, 0, 0, 2, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_FALSE(game.isNotEnd());
}

TEST(GameStateTest, BoardTest) {
    GameState game;
    CheckersBoard board = {
        1, 0, 2, 0, 1, 0, 2, 0,
        0, 1, 0, 2, 0, 1, 0, 2,
       11, 0,12, 0,11, 0,12, 0,
        0,12, 0,11, 0,12, 0,11,
       11, 0, 1, 0,11, 0, 1, 0,
        0, 2, 0,12, 0, 1, 0,11,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    game.init(1, board);
    EXPECT_TRUE(game.isPlayerMove(1));
    EXPECT_TRUE(sameArrays(game.getBoard(), board));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
