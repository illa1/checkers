# Checkers game


Core of the game - minmax algorithm and rules - are written in *C++*. For tests *gtest* library is used.

Very simple GUI is made with *Python* (both versions: 2 and 3 are supported) and it uses *PyQt5* library.

These two parts communicate with each other by *stdin* and *stdout*.

![checkers](/uploads/2310b2dd2c3443b42f6b5c56e7efa5cb/checkers.png)