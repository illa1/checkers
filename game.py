from threading import Thread
from statistics import saveJsonInFile, fromFileToJson
import subprocess

MINMAX_LEVELS = {
    1: 2,
    2: 4,
    3: 7,
    4: 8,
}

class Game(object):

    def __init__(self, player_id, level):
        self.grid = [[] for i in range(8)]
        self.player_move = False
        self.player_id = player_id
        self.level = level
        self.process = None
        self.board = None
        self.ended = True

    def run_process(self):
        level1, level2 = 0, 0
        if self.player_id == 1:
            level2 = MINMAX_LEVELS[self.level]
        else:
            level1 = MINMAX_LEVELS[self.level]
        self.process = subprocess.Popen(["./checkers", str(level1), str(level2)], shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True, bufsize=1)

    def save_game_result(self, result):
        results = fromFileToJson('statistics.json')
        for r in results:
            if r['level'] == self.level:
                r[result] += 1
                break
        saveJsonInFile(results, 'statistics.json')

    def start_new_game(self, changeColor):
        if not self.ended:
            self.save_game_result('lost')
        self.ended = False
        if changeColor:
            self.player_id = 1 if self.player_id == 2 else 2
        if self.process is not None:
            self.process.kill()
        self.current_field = None
        self.run_process()
        self.parseInput()

    def parseInput(self):
        line = list(map(int, self.process.stdout.readline().split()))
        print(line)
        if line == []:
            return
        turn = line.pop(0)
        if turn < 10 and turn != self.player_id:
            print('print OK')
            self.process.stdin.write("OK\n")
        for i in range(8):
            self.grid[i] = line[i*8:i*8+8]
        if self.board is not None:
            self.board.update()

        if turn > 10:
            print('end')
            results = fromFileToJson('statistics.json')
            print('wygral: ', turn % 10)
            if turn % 10 == self.player_id:
                self.save_game_result('won')
            else:
                self.save_game_result('lost')
            self.ended = True
            return
        if turn != self.player_id:
            self.player_move = False
            thread = Thread(target=self.parseInput)
            thread.start()
        else:
            self.player_move = True

    def makeMove(self, x1, y1, x2, y2):
        if self.player_id == 1:
            x1, y1, x2, y2 = 7-x1, 7-y1, 7-x2, 7-y2
        self.process.stdin.write("{} {} {} {}\n".format(x1, y1, x2, y2))
        self.current_field = None
        if self.check_if_valid():
            self.parseInput()
        else:
            self.board.update()

    def check_field(self, x, y):
        if self.current_field is None:
            self.check(x, y)
        else:
            end_pos = [x, y]
            self.makeMove(self.current_field[0], self.current_field[1], end_pos[0], end_pos[1])
            self.current_field = None

    def check(self, x1, y1):
        x, y = x1, y1
        if self.player_id == 1:
            x, y = 7-x1, 7-y1
        if self.grid[y][x] % 10 != self.player_id:
            self.current_field = None
            return
        self.current_field = [x1, y1]
        self.board.update()

    def check_if_valid(self):
        if self.process.stdout.readline() == 'OK\n':
            return True
        return False

    def close(self):
        self.process.kill()
